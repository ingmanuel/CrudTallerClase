namespace BaseDatosEntityFramework.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Direccion
    {
       

        [Key]
        public int CodigoDireccion { get; set; }

        public string Barrio { get; set; }

        public string Calle { get; set; }

        public int Persona_Id { get; set; }

        public virtual Persona Persona { get; set; }
        public  virtual List<SubDireccion> SubDireccion { get; set; }
    }
}
