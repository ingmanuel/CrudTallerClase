namespace BaseDatosEntityFramework.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.Infrastructure;

    public partial class ApplicationDbContext : DbContext
    {
       
        public virtual DbSet<Direccion> Direccions { get; set; }
        public virtual DbSet<Persona> Personas { get; set; }
        public virtual DbSet<SubDireccion> SubDireccions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Direccion>()
                .HasMany(e => e.SubDireccion)
                .WithRequired(e => e.Direccion)
                .HasForeignKey(e => e.Direccion_CodigoDireccion);

            modelBuilder.Entity<Persona>()
                .HasMany(e => e.Direccions)
                .WithRequired(e => e.Persona)
                .HasForeignKey(e => e.Persona_Id);

           
        }

        protected override bool ShouldValidateEntity(DbEntityEntry entityEntry)
        {
            if (entityEntry.State == EntityState.Deleted)
            {
                return true;
            }

            return base.ShouldValidateEntity(entityEntry);
        }

       
    }
}
