namespace BaseDatosEntityFramework.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Persona
    {
        public int Id { get; set; }
        public int Edad { get; set; }
        public string Nombre { get; set; }
        public DateTime Nacimiento { get; set; }
        public int Sexo { get; set; }
        public virtual List<Direccion> Direccions { get; set; }
    }
}
