﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseDatosEntityFramework.Models
{
    public class IndexViewModel: BaseModels
    {
        public List<Persona> Personas { get; set; }
    }
}