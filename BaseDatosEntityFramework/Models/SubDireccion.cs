namespace BaseDatosEntityFramework.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SubDireccion
    {
        public int id { get; set; }

        [StringLength(124)]
        public string subCalle { get; set; }

        public int Direccion_CodigoDireccion { get; set; }

        public virtual Direccion Direccion { get; set; }
    }
}
