﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseDatosEntityFramework.Models
{
    public class BaseModels
    {
        public int PaginaActual { get; set; }
        public int TotalDeRegistros { get; set; }
        public int RegistroPorPagina { get; set; }
    }
}