﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseDatosEntityFramework.Models;

namespace BaseDatosEntityFramework.Controllers
{
    public class DireccionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Direccions
        public ActionResult Index()
        {
           
            var direccions = db.Direccions.Include(d => d.Persona);
            return View(direccions.ToList());
        }

        // GET: Direccions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Direccion direccion = db.Direccions.Find(id);
            if (direccion == null)
            {
                return HttpNotFound();
            }
            return View(direccion);
        }

        // GET: Direccions/Create
        public ActionResult Create()
        {
            ViewBag.Persona_Id = new SelectList(db.Personas, "Id", "Nombre");
            return View();
        }

        // POST: Direccions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CodigoDireccion,Barrio,Calle,Persona_Id")] Direccion direccion)
        {
            if (ModelState.IsValid)
            {
                var direcion = new List<Direccion>() { direccion };
                for (int y=0; y<1000; y++) {
                    var direcion1 = new Direccion();
                    direcion1.CodigoDireccion = 0;
                    direcion1.Barrio = "Pando";
                    direcion1.Calle = "30";
                    direcion1.Persona_Id = 2;
                    direcion.Add(direcion1);
                }
                direcion.Add(direccion);
                db.Direccions.AddRange(direcion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Persona_Id = new SelectList(db.Personas, "Id", "Nombre", direccion.Persona_Id);
            return View(direccion);
        }

        // GET: Direccions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Direccion direccion = db.Direccions.Find(id);
            if (direccion == null)
            {
                return HttpNotFound();
            }
            ViewBag.Persona_Id = new SelectList(db.Personas, "Id", "Nombre", direccion.Persona_Id);
            return View(direccion);
        }

        // POST: Direccions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CodigoDireccion,Barrio,Calle,Persona_Id")] Direccion direccion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(direccion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Persona_Id = new SelectList(db.Personas, "Id", "Nombre", direccion.Persona_Id);
            return View(direccion);
        }

        // GET: Direccions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Direccion direccion = db.Direccions.Find(id);
            if (direccion == null)
            {
                return HttpNotFound();
            }
            return View(direccion);
        }

        // POST: Direccions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Direccion direccion = db.Direccions.Find(id);
            db.Direccions.Remove(direccion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
