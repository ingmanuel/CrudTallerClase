﻿using BaseDatosEntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseDatosEntityFramework.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int pagina = 1)
        {
            var cantidadRegistro = 12;
            using (var db = new ApplicationDbContext())
            {

                var personas = db.Personas.OrderBy(x => x.Id)
                 .Skip((pagina - 1) * cantidadRegistro)
                 .Take(cantidadRegistro).ToList();

                var totalDeRegistro = db.Personas.Count();

                var modelo = new IndexViewModel();

                modelo.Personas = personas;
                modelo.PaginaActual = pagina;
                modelo.TotalDeRegistros = totalDeRegistro;
                modelo.RegistroPorPagina = cantidadRegistro;
                return View(modelo);
            }
           
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}