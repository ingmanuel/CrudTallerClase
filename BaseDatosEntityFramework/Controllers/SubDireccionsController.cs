﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseDatosEntityFramework.Models;

namespace BaseDatosEntityFramework.Controllers
{
    public class SubDireccionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SubDireccions
        public ActionResult Index()
        {
            var subDireccions = db.SubDireccions.Include(s => s.Direccion);
            return View(subDireccions.ToList());
        }

        // GET: SubDireccions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubDireccion subDireccion = db.SubDireccions.Find(id);
            if (subDireccion == null)
            {
                return HttpNotFound();
            }
            return View(subDireccion);
        }

        // GET: SubDireccions/Create
        public ActionResult Create()
        {
            ViewBag.Direccion_CodigoDireccion = new SelectList(db.Direccions, "CodigoDireccion", "Barrio");
            return View();
        }

        // POST: SubDireccions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,subCalle,Direccion_CodigoDireccion")] SubDireccion subDireccion)
        {
            if (ModelState.IsValid)
            {
                db.SubDireccions.Add(subDireccion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Direccion_CodigoDireccion = new SelectList(db.Direccions, "CodigoDireccion", "Barrio", subDireccion.Direccion_CodigoDireccion);
            return View(subDireccion);
        }

        // GET: SubDireccions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubDireccion subDireccion = db.SubDireccions.Find(id);
            if (subDireccion == null)
            {
                return HttpNotFound();
            }
            ViewBag.Direccion_CodigoDireccion = new SelectList(db.Direccions, "CodigoDireccion", "Barrio", subDireccion.Direccion_CodigoDireccion);
            return View(subDireccion);
        }

        // POST: SubDireccions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,subCalle,Direccion_CodigoDireccion")] SubDireccion subDireccion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subDireccion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Direccion_CodigoDireccion = new SelectList(db.Direccions, "CodigoDireccion", "Barrio", subDireccion.Direccion_CodigoDireccion);
            return View(subDireccion);
        }

        // GET: SubDireccions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubDireccion subDireccion = db.SubDireccions.Find(id);
            if (subDireccion == null)
            {
                return HttpNotFound();
            }
            return View(subDireccion);
        }

        // POST: SubDireccions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubDireccion subDireccion = db.SubDireccions.Find(id);
            db.SubDireccions.Remove(subDireccion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
